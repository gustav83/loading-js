# Plugin <Un-named>
# By <your name> <year>
#
# <Notes>
#
# License: <MIT, Public Domain, ...?>
# 
# Started from https://github.com/alanhogan/Coffeescript-jQuery-Plugin-Template
String::repeat = (num) ->
  new Array(num + 1).join this

(($, window) ->
  $.extend $.fn, loading: (options) ->
    @defaultOptions =
      class: 'rects'
      prefix: 'loading-'
      templates:
        rects: '<p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p>'
        cubes: '<p><i></i><i></i><i></i><i></i><i></i><i></i></p>'.repeat(27)
    settings = $.extend({}, @defaultOptions, options)

    @each (i, element) =>
      template = settings.class.split(' ')[0]
      $element = $(element)
      $element.addClass(settings.prefix + settings.class)
      $element.html(settings.templates[template])
    @ # allow chaining
) this.jQuery or this.Zepto, this